const fs = require("fs");

const { FILE_FORMAT } = require("./src/common");

const TelemetryManager = require("./src/manager");

const inputFile = process.argv[2] ?? "inputs.txt";

fs.readFile(inputFile, FILE_FORMAT, (err, fileContent) => {
  if (err) {
    console.log("Unable to read the input file", err);
    return;
  }

  const records = fileContent.split(/\r?\n/);
  const alerts = [];
  const telemetry = new TelemetryManager();

  // stream telemetry data one at a time
  records.forEach((record) => {
    if (record.trim().length === 0) return;

    // store the latest telemetry in the store
    const { satelliteId, component } = telemetry.ingest(record);

    // determine if any alerts are triggered in the last 5 minutes
    if (telemetry.alert(satelliteId, component)) {
      alerts.push(telemetry.createAlert(satelliteId, component));
    }
  });

  // show all the alerts to the console
  console.log(alerts);
});
