/**
 * Supported file encoding format, e.g. ascii, utf8
 */
const FILE_FORMAT = "ascii";

/**
 * Five minutes for the default interval
 */
const DEFAULT_INTERVAL = 5 * 60 * 1000;

/**
 * Number of violations in an interval that will trigger alert message.
 */
const NUM_ALERTS_TRIGGER = 3;

module.exports = { FILE_FORMAT, DEFAULT_INTERVAL, NUM_ALERTS_TRIGGER };
