
/**
 * Components enumerations mapping to the abbreviations found in the telemetry data.
 */
const Components = {
  BATTERY: "BATT",
  THERMOSTAT: "TSTAT",
}

/**
 * Functions that checks to see if the record has alert condition.
 */
const ComponentAlertFunctions = {
  [Components.BATTERY]: (limits, value) => value < limits.low,
  [Components.THERMOSTAT]: (limits, value) => value > limits.high,
};

/**
 * Alert severity types for different satellite components.
 */
const ComponentAlertSeverity = {
  [Components.BATTERY]: "RED LOW",
  [Components.THERMOSTAT]: "RED HIGH",
};

module.exports = { ComponentAlertFunctions, ComponentAlertSeverity };
