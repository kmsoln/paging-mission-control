const TelemetryRecord = require("./record");
const { ComponentAlertSeverity } = require("./components");
const { NUM_ALERTS_TRIGGER } = require("./common");

/**
 * Two-level store that keeps track of satellites and their component's telemetry:
 *   { satelliteId: { component: [...] } }
 * Telemetry records for each component will be stored in a queue-order (most-recent first).
 */
function TelemetryManager() {
  this.store = {};
}

/**
 * Adds the telemetry record into the appropriate satellite's telemetry store.
 * Assumes the data is properly formatted.
 *
 * @param {string} data ascii pipe delimited telemetry record
 * @returns parsed telemetry record
 */
TelemetryManager.prototype.ingest = function (data) {
  // parse and create a new record object
  const telemetryRecord = new TelemetryRecord(data);
  const { satelliteId, component, time: latestTime } = telemetryRecord;

  // telemetry store for this satellite
  let satelliteTelemetry = this.store[satelliteId];

  // satellite seen the first time, initialize a telemetry store
  if (!satelliteTelemetry) {
    satelliteTelemetry = { [component]: [telemetryRecord] };
    this.store[satelliteId] = satelliteTelemetry;
    return telemetryRecord;
  }

  // component seen the first time, initialize a queue to keep all the telemetry records
  if (!satelliteTelemetry[component]) {
    satelliteTelemetry[component] = [telemetryRecord];
  } else {
    // evict any records that are outside of the interval
    satelliteTelemetry[component] = satelliteTelemetry[component].filter(
      (record) => !record.insideInterval(latestTime)
    );

    // override the most recent entry if the telemetry record was logged at the same time
    if (satelliteTelemetry[component][0]?.time === latestTime) {
      satelliteTelemetry[component][0] = telemetryRecord;
    } else {
      satelliteTelemetry[component].unshift(telemetryRecord);
    }
  }

  return telemetryRecord;
};

/**
 * Determines whether the satellite's component has any alerts.
 * @param {number} satelliteId satellite id
 * @param {string} component component name
 * @returns true if there is an alert for this component
 */
TelemetryManager.prototype.alert = function (satelliteId, component) {
  const intervalRecords = this.store[satelliteId]?.[component] ?? [];

  if (intervalRecords.length < NUM_ALERTS_TRIGGER) {
    return false;
  }

  let intervalAlerts = 0;

  for (let record of intervalRecords) {
    intervalAlerts = intervalAlerts + record.hasAlert;
    if (intervalAlerts === NUM_ALERTS_TRIGGER) {
      return true;
    }
  }

  return false;
};


/**
 * Creates an alert message based on the most recent telemetry
 * for the satellite's component.
 * @param {number} satelliteId satellite id
 * @param {string} component component name
 * @returns alert message for this component
 */
TelemetryManager.prototype.createAlert = function (satelliteId, component) {
  const { timestamp } = this.store[satelliteId][component][0];

  return {
    satelliteId,
    severity: ComponentAlertSeverity[component],
    component,
    timestamp,
  };
};

module.exports = TelemetryManager;
