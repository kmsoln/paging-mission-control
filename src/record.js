const { ComponentAlertFunctions } = require("./components");
const { DEFAULT_INTERVAL } = require("./common");
const { convertDatetime } = require("./time");

/**
 * Parses status telemetry data that's pipe delimited.
 * @param {string} data pipe delimited status telemetry string
 * @param {number} interval (optional) time interval to compute expiry
 */
function TelemetryRecord(data, interval = DEFAULT_INTERVAL) {
  const [
    _timestamp,
    _satelliteId,
    _redHigh,
    _yellowHigh,
    _yellowLow,
    _redLow,
    _value,
    _component,
  ] = data.split("|");

  // convert timestamp to ISO date and time format
  const { time, timestamp } = convertDatetime(_timestamp);
  this.time = time;
  this.timestamp = timestamp;

  this.satelliteId = parseInt(_satelliteId);
  this.component = _component;

  this.red = { low: parseFloat(_redLow), high: parseFloat(_redHigh) };
  this.yellow = { low: parseFloat(_yellowLow), high: parseFloat(_yellowHigh) };
  this.value = parseFloat(_value);

  // pre-compute the alert indication for this record
  const checkAlert = ComponentAlertFunctions[_component];
  this.hasAlert = checkAlert ? checkAlert(this.red, this.value) : false;

  // pre-compute the expiry of this record
  this.expiry = time + interval;
}

/**
 * Indicates if this record is still inside the interval determined
 * by the latest telemetry time and the interval duration.
 * 
 * @param {number} latestTime epoch time of the latest telemetry
 * @returns true if this record is still inside the interval
 */

TelemetryRecord.prototype.insideInterval = function (latestTime) {
  return latestTime > this.expiry;
};

module.exports = TelemetryRecord;
