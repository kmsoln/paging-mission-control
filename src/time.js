
/**
 * Converts `YYYYMMDD hh:mm:ss.s` format string to ISO date time format and unix epoch time.
 * @param {string} dt timestamp string in YYYYMMDD hh:mm:ss.s format
 * @param {string} timezone timezone for this timestamp
 * @returns unix epoch time and ISO-formatted timestamp
 */
function convertDatetime(dt, timezone = "Z") {
  const year = dt.slice(0, 4); // YYYY
  const month = dt.slice(4, 6); // MM
  const day = dt.slice(6, 8); // DD
  const time = dt.slice(9); // hh:mm:ss.s

  const timestamp = `${year}-${month}-${day}T${time}${timezone}`;

  return { time: new Date(timestamp).getTime(), timestamp };
}

module.exports = { convertDatetime };
